from flask import Flask
from flask import request
from flask import render_template
from metlinkpid import PID
from threading import Thread, Event, Lock

pid = PID.for_device('/dev/ttyUSB0')

pid_lock = Lock()
ping_event = Event()


def ping():
    while True:
        with pid_lock:
            pid.ping()
        if ping_event.wait(10):
            break


Thread(target=ping).start()

app = Flask(__name__)


@app.route('/')
def my_form():
    return render_template("home.html")  # this should be the name of your html file


@app.route('/', methods=['POST'])
def my_form_post():
    type = request.form['type']
    line1 = request.form['line1']
    line2 = request.form['line2']
    scroll = request.form['scroll']
    FixedText = request.form.get("FixedText")
    StringEntry = request.form.get("StringEntry")
    #print(type)
    #print(line1)
    #print(line2)
    #print(FixedText)
    #print(StringEntry)
    

    if type == "FreeText":
            print("This is FreeText")
            
            if scroll == "":
                PIDMessage = (line1 + "_" + line2)

            else:
                PIDMessage = (line1 + "_" + line2 + "|_" + scroll)
            

            print(PIDMessage)
            pid.send(PIDMessage)
    elif type == "Fixed":
            print("This is a Fixed Message")
            print(FixedText)
            pid.send(FixedText)

    elif type == "StringEntry":
            print("This is a String Entry")
            print(StringEntry)
            pid.send(StringEntry)

    elif type == "Departure":
            print("Departure Script to be run")

    elif type == "Blank":
            print("Blanking PID")
            pid.send("")
            
            
    else:
            print ("Error")


    return render_template("home.html")  # this should be the name of your html file


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
    ping_event.set()
