from threading import Thread, Event, Lock

from flask import Flask, request, jsonify
from metlinkpid import PID

pid = PID.for_device('/dev/ttyUSB0')
pid_lock = Lock()
ping_event = Event()


def ping():
    while True:
        with pid_lock:
            pid.ping()
        if ping_event.wait(10):
            break


Thread(target=ping).start()

app = Flask(__name__)


@app.route("/")
def hello():
    message = request.args.get('message')
    json = {'message': message}
    if message:
        try:
            with pid_lock:
                pid.send(message)
            json['error'] = None
        except Exception as e:
            json['error'] = str(e)
    else:
        json['error'] = "provide a 'message' parameter"
    return jsonify(json)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
    ping_event.set()
