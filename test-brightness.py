from time import sleep
from metlinkpid import PID, PingMessage, DisplayMessage
from sys import stderr

tmpl_bytes = DisplayMessage.from_str('N0^X_████████████████████').to_bytes()

with PID.for_device('/dev/ttyUSB0') as pid:
    for i in range(0x50, 256):
        inst_bytes = tmpl_bytes.replace(b'X', '0x{:02X}'.format(i).encode('ascii'))
        try:
            pid.send(inst_bytes)
            sleep(0.5)
            pid.send(PingMessage(i))
        except Exception as e:
            print(e, file=stderr)
        # pid.send('N0^███ ' + str(i) + ' ' + '0x{:02X}'.format(i) + ' ███')
        # pid.send(PingMessage(i))
        input()




